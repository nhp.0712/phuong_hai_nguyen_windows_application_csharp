﻿/*  
    File:           TShirt_Order_Form.cs
    Programmer:     Phuong Nguyen
    Date:           February 22, 2019
    Purpose:        Create a T-shirt order processing application following the guidelines for Programming Exercise #2 on p. 695
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NguyenProgram4
{
    public partial class TShirt_Order_Form : Form
    {
        public TShirt_Order_Form()
        {
            InitializeComponent();
        }
        // Function: exitToolStripMenuItem_Click
        // Type: void
        // Exit option closes the application once clicked on Exit item of Process Menu
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
        // Function: displayOrderToolStripMenuItem_Click
        // Type: void
        // Display Order option displays order info (all user entries/selections) once clicked on Display Order of Process Menu
        private void displayOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Using if/else if statements to check if radioButton is selected or not, then retrieve the selected data.
            string sChecked = "";
            if (rSmall.Checked)
                sChecked = rSmall.Text;
            else if (rMedium.Checked)
                sChecked = rMedium.Text;
            else if (rLarge.Checked)
                sChecked = rLarge.Text;
            else if (rExtraLarge.Checked)
                sChecked = rExtraLarge.Text;
            tDisplay.Visible = true;
            // Collecting all user's information and selection to display.
            this.tDisplay.Text = tName.Text + Environment.NewLine 
                                + tPhone.Text + Environment.NewLine 
                                + tEmail.Text + Environment.NewLine + Environment.NewLine 
                                + "Size: " + sChecked + Environment.NewLine 
                                + "Quantity: " + comQuantity.Text;
        }

        private void tDisplay_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
