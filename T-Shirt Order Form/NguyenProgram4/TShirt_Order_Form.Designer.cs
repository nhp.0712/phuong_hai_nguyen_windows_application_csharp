﻿/*  
    File:           TShirt_Order_Form.Designer.cs
    Programmer:     Phuong Nguyen
    Date:           February 22, 2019
    Purpose:        Containing all functions and contents of the Windows Form Application
*/

namespace NguyenProgram4
{
    partial class TShirt_Order_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TShirt_Order_Form));
            this.gCustomer = new System.Windows.Forms.GroupBox();
            this.tEmail = new System.Windows.Forms.TextBox();
            this.tPhone = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.laEmail = new System.Windows.Forms.Label();
            this.laPhone = new System.Windows.Forms.Label();
            this.laName = new System.Windows.Forms.Label();
            this.gSize = new System.Windows.Forms.GroupBox();
            this.rExtraLarge = new System.Windows.Forms.RadioButton();
            this.rLarge = new System.Windows.Forms.RadioButton();
            this.rMedium = new System.Windows.Forms.RadioButton();
            this.rSmall = new System.Windows.Forms.RadioButton();
            this.gDisplayInfo = new System.Windows.Forms.GroupBox();
            this.tDisplay = new System.Windows.Forms.TextBox();
            this.comQuantity = new System.Windows.Forms.ComboBox();
            this.laQuantity = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.processToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gCustomer.SuspendLayout();
            this.gSize.SuspendLayout();
            this.gDisplayInfo.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Type: groupBox   
            // Name: gCustomer
            // Function: Containing all controls to retrieve user's information (Name/Phone/Email). 
            //  
            this.gCustomer.Controls.Add(this.tEmail);
            this.gCustomer.Controls.Add(this.tPhone);
            this.gCustomer.Controls.Add(this.tName);
            this.gCustomer.Controls.Add(this.laEmail);
            this.gCustomer.Controls.Add(this.laPhone);
            this.gCustomer.Controls.Add(this.laName);
            this.gCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.gCustomer.Location = new System.Drawing.Point(12, 27);
            this.gCustomer.Name = "gCustomer";
            this.gCustomer.Size = new System.Drawing.Size(398, 133);
            this.gCustomer.TabIndex = 0;
            this.gCustomer.TabStop = false;
            this.gCustomer.Text = "Customer";
            // 
            // Type: textBox   
            // Name: tEmail
            // Function: Getting user's email 
            //  
            this.tEmail.Location = new System.Drawing.Point(186, 95);
            this.tEmail.Name = "tEmail";
            this.tEmail.Size = new System.Drawing.Size(190, 23);
            this.tEmail.TabIndex = 6;
            // 
            // Type: textBox   
            // Name: tPhone
            // Function: Getting user's phone 
            // 
            this.tPhone.Location = new System.Drawing.Point(186, 62);
            this.tPhone.Name = "tPhone";
            this.tPhone.Size = new System.Drawing.Size(190, 23);
            this.tPhone.TabIndex = 5;
            // 
            // Type: textBox   
            // Name: tName
            // Function: Getting user's Name 
            // 
            this.tName.Location = new System.Drawing.Point(186, 29);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(190, 23);
            this.tName.TabIndex = 4;
            // 
            // Type: label
            // Name: laEmail
            // Function: Displays the title of Email entry
            // 
            this.laEmail.AutoSize = true;
            this.laEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.laEmail.Location = new System.Drawing.Point(12, 102);
            this.laEmail.Name = "laEmail";
            this.laEmail.Size = new System.Drawing.Size(50, 17);
            this.laEmail.TabIndex = 2;
            this.laEmail.Text = "Email: ";
            // 
            // Type: label
            // Name: laPhone
            // Function: Displays the title of Phone entry
            // 
            this.laPhone.AutoSize = true;
            this.laPhone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.laPhone.Location = new System.Drawing.Point(12, 65);
            this.laPhone.Name = "laPhone";
            this.laPhone.Size = new System.Drawing.Size(57, 17);
            this.laPhone.TabIndex = 1;
            this.laPhone.Text = "Phone: ";
            // 
            // Type: label
            // Name: laName
            // Function: Displays the title of Name entry
            // 
            this.laName.AutoSize = true;
            this.laName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.laName.Location = new System.Drawing.Point(12, 32);
            this.laName.Name = "laName";
            this.laName.Size = new System.Drawing.Size(49, 17);
            this.laName.TabIndex = 0;
            this.laName.Text = "Name:";
            // 
            // Type: groupBox   
            // Name: gSize
            // Function: Containing all controls to retrieve user's selection(T-Shirt size). 
            //  
            this.gSize.Controls.Add(this.rExtraLarge);
            this.gSize.Controls.Add(this.rLarge);
            this.gSize.Controls.Add(this.rMedium);
            this.gSize.Controls.Add(this.rSmall);
            this.gSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.gSize.Location = new System.Drawing.Point(12, 166);
            this.gSize.Name = "gSize";
            this.gSize.Size = new System.Drawing.Size(398, 130);
            this.gSize.TabIndex = 1;
            this.gSize.TabStop = false;
            this.gSize.Text = "Size:";
            // 
            // Type: radioButtion   
            // Name: rExtraLarge
            // Function: Displaying the size option, in here "Extra Large" 
            //  
            this.rExtraLarge.AutoSize = true;
            this.rExtraLarge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.rExtraLarge.Location = new System.Drawing.Point(15, 97);
            this.rExtraLarge.Name = "rExtraLarge";
            this.rExtraLarge.Size = new System.Drawing.Size(99, 21);
            this.rExtraLarge.TabIndex = 6;
            this.rExtraLarge.TabStop = true;
            this.rExtraLarge.Text = "Extra Large";
            this.rExtraLarge.UseVisualStyleBackColor = true;
            // 
            // Type: radioButtion   
            // Name: rLarge
            // Function: Displaying the size option, in here "Large" 
            // 
            this.rLarge.AutoSize = true;
            this.rLarge.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.rLarge.Location = new System.Drawing.Point(15, 74);
            this.rLarge.Name = "rLarge";
            this.rLarge.Size = new System.Drawing.Size(63, 21);
            this.rLarge.TabIndex = 5;
            this.rLarge.TabStop = true;
            this.rLarge.Text = "Large";
            this.rLarge.UseVisualStyleBackColor = true;
            // 
            // Type: radioButtion   
            // Name: rMedium
            // Function: Displaying the size option, in here "Medium" 
            // 
            this.rMedium.AutoSize = true;
            this.rMedium.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.rMedium.Location = new System.Drawing.Point(15, 51);
            this.rMedium.Name = "rMedium";
            this.rMedium.Size = new System.Drawing.Size(75, 21);
            this.rMedium.TabIndex = 4;
            this.rMedium.TabStop = true;
            this.rMedium.Text = "Medium";
            this.rMedium.UseVisualStyleBackColor = true;
            // 
            // Type: radioButtion   
            // Name: rSmall
            // Function: Displaying the size option, in here "Small" 
            // 
            this.rSmall.AutoSize = true;
            this.rSmall.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.rSmall.Location = new System.Drawing.Point(15, 28);
            this.rSmall.Name = "rSmall";
            this.rSmall.Size = new System.Drawing.Size(60, 21);
            this.rSmall.TabIndex = 3;
            this.rSmall.TabStop = true;
            this.rSmall.Text = "Small";
            this.rSmall.UseVisualStyleBackColor = true;
            // 
            // Type: groupBox   
            // Name: gSize
            // Function: Containing all controls to display user's selection and information. 
            //  
            this.gDisplayInfo.Controls.Add(this.tDisplay);
            this.gDisplayInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.gDisplayInfo.Location = new System.Drawing.Point(12, 357);
            this.gDisplayInfo.Name = "gDisplayInfo";
            this.gDisplayInfo.Size = new System.Drawing.Size(398, 186);
            this.gDisplayInfo.TabIndex = 0;
            this.gDisplayInfo.TabStop = false;
            this.gDisplayInfo.Text = "Order Info:";
            // 
            // Type: textBox   
            // Name: tDisplay
            // Function: Collects all user's selection and information then displays once Clicked on Dispay Order of MenuStrip  
            // 
            this.tDisplay.Cursor = System.Windows.Forms.Cursors.No;
            this.tDisplay.Location = new System.Drawing.Point(15, 34);
            this.tDisplay.Multiline = true;
            this.tDisplay.Name = "tDisplay";
            this.tDisplay.ReadOnly = true;
            this.tDisplay.Size = new System.Drawing.Size(370, 134);
            this.tDisplay.TabIndex = 0;
            this.tDisplay.Visible = false;
            this.tDisplay.TextChanged += new System.EventHandler(this.tDisplay_TextChanged);
            // 
            // Type: comboBox   
            // Name: comQuantity
            // Function: Containing all options for Quantity
            //
            this.comQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.comQuantity.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comQuantity.Location = new System.Drawing.Point(198, 310);
            this.comQuantity.Name = "comQuantity";
            this.comQuantity.Size = new System.Drawing.Size(212, 25);
            this.comQuantity.TabIndex = 2;
            // 
            // Type: label
            // Name: laQuantity
            // Function: Displays the title of Quantity entry
            // 
            this.laQuantity.AutoSize = true;
            this.laQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.laQuantity.Location = new System.Drawing.Point(24, 310);
            this.laQuantity.Name = "laQuantity";
            this.laQuantity.Size = new System.Drawing.Size(65, 17);
            this.laQuantity.TabIndex = 3;
            this.laQuantity.Text = "Quantity:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.processToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(420, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // processToolStripMenuItem
            // 
            this.processToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayOrderToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.processToolStripMenuItem.Name = "processToolStripMenuItem";
            this.processToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.processToolStripMenuItem.Text = "Process";
            // 
            // displayOrderToolStripMenuItem
            // 
            this.displayOrderToolStripMenuItem.Name = "displayOrderToolStripMenuItem";
            this.displayOrderToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.displayOrderToolStripMenuItem.Text = "Display Order";
            this.displayOrderToolStripMenuItem.Click += new System.EventHandler(this.displayOrderToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // TShirt_Order_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(420, 549);
            this.Controls.Add(this.laQuantity);
            this.Controls.Add(this.comQuantity);
            this.Controls.Add(this.gDisplayInfo);
            this.Controls.Add(this.gSize);
            this.Controls.Add(this.gCustomer);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TShirt_Order_Form";
            this.Text = "T-Shirt Order Form";
            this.gCustomer.ResumeLayout(false);
            this.gCustomer.PerformLayout();
            this.gSize.ResumeLayout(false);
            this.gSize.PerformLayout();
            this.gDisplayInfo.ResumeLayout(false);
            this.gDisplayInfo.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gCustomer;
        private System.Windows.Forms.TextBox tEmail;
        private System.Windows.Forms.TextBox tPhone;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.Label laEmail;
        private System.Windows.Forms.Label laPhone;
        private System.Windows.Forms.Label laName;
        private System.Windows.Forms.GroupBox gSize;
        private System.Windows.Forms.RadioButton rLarge;
        private System.Windows.Forms.RadioButton rMedium;
        private System.Windows.Forms.RadioButton rSmall;
        private System.Windows.Forms.GroupBox gDisplayInfo;
        private System.Windows.Forms.ComboBox comQuantity;
        private System.Windows.Forms.Label laQuantity;
        private System.Windows.Forms.RadioButton rExtraLarge;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem processToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox tDisplay;
    }
}

