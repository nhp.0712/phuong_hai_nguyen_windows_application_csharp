﻿using System.Drawing;

namespace NguyenProgram6
{
    public static class ImageFilter
    {
        public static Image ApplyBlueFilter(this Image inputImage)
        {
            //Create a new bitmap object with the dimensions of the original image
            Bitmap outputImage = new Bitmap(inputImage.Width, inputImage.Height);
            //Create a graphics variable from the outputImage created above
            Graphics imageGraphics = Graphics.FromImage(outputImage);
            //Fill the imageGraphics variable with the input (original) image
            imageGraphics.DrawImage(inputImage, 5, 5);

            // Apply a blue filter by brushing with ARGB color
            imageGraphics.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Blue)), 0, 0, outputImage.Width, outputImage.Height);


            //Return the filtered image
            return outputImage;

        }
    }
}
