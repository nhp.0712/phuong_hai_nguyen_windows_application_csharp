﻿/*  
    File:           Image_Filter.cs
    Programmer:     Phuong Nguyen
    Date:           March 27, 2019
    Purpose:        create a class that applies an colored filter to an image, include a Windows Form, a class, and an image resource
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NguyenProgram6
{
    public partial class Image_Filter : Form
    {
        public Image_Filter()
        {
            InitializeComponent();
        }

        // Type: void
        // Name: picOriginal_Click
        // Applying blue filter on the image once clicked.
        private void picOriginal_Click(object sender, EventArgs e)
        {
            picOriginal.Image = picOriginal.Image.ApplyBlueFilter();
        }
    }
}
