﻿// Intramural.cs
using System;
// OrganizationNamespace added to avoid fully qualifying
// references
using OrganizationNamespace;

namespace IntramuralNamespace
{
    public class Intramural : Organization
    {
        private string sportType;

        public Intramural(string name, string pContact, string sport)
            // call to Organization (base) class constructor
            : base(name, pContact)
        {
            sportType = sport;
        }

        // Default constructor
        public Intramural()
        {
            sportType = "unknown";
        }

        // Property for sportType
        public string SportType
        {
            get
            {
                return sportType;
            }
            set
            {
                sportType = value;
            }
        }
    }
}
