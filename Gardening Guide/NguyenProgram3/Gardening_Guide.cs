﻿/*  
    File:           Gardening_Guide.cs
    Programmer:     Phuong Nguyen
    Date:           February 15, 2019
    Purpose:        Create the Gardening Guide application illustrated by Figure 10-8 on p. 617
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NguyenProgram3
{
    public partial class Gardening_Guide : Form
    {
        public Gardening_Guide()
        {
            InitializeComponent();
        }
        // Function: Combo_Flowers_SelectedIndexChanged
        // Type: void
        // Retrieve the selected item from Combo_Flowers then display the result in the TextBox_ComboFlowers.
        private void Combo_Flowers_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TextBox_ComboFlowers.Text = Combo_Flowers.Text;
        }
        // Function: List_Trees_SelectedIndexChanged
        // Type: void
        // Retrieve the selected item(s) from textbox 'TextBox_ListTrees' then display a message.
        private void List_Trees_SelectedIndexChanged(object sender, EventArgs e)
        {

            string result = " ";
            // Using foreach loop to retrieve all the selected item(s), then initialize the data into a string result that is displayed in TextBox_ListTrees.
            foreach (string activity in List_Trees.SelectedItems)
            {
                result += activity + "    ";
                this.TextBox_ListTrees.Text = result;
            }
        }
    }
}
