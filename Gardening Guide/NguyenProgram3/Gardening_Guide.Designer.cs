﻿/*  
    File:           Gardening_Guide.Designer.cs
    Programmer:     Phuong Nguyen
    Date:           February 15, 2019
    Purpose:        Containing all functions and contents of the Windows Form Application
*/

namespace NguyenProgram3
{
    partial class Gardening_Guide
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Combo_Flowers = new System.Windows.Forms.ComboBox();
            this.List_Trees = new System.Windows.Forms.ListBox();
            this.Label_MYS = new System.Windows.Forms.Label();
            this.Lable_Flowers = new System.Windows.Forms.Label();
            this.Lable_Trees = new System.Windows.Forms.Label();
            this.Lable_FlowersPref = new System.Windows.Forms.Label();
            this.Lable_TreesPref = new System.Windows.Forms.Label();
            this.TextBox_ComboFlowers = new System.Windows.Forms.TextBox();
            this.TextBox_ListTrees = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Type: comboBox   
            // Name: Combo_Flowers
            // Function: Containing all items for Flowers - DropDownList style. 
            //           Once clicked on an item, event_Handler will retrieve and display the item in TextBox_FlowersCombo
            // 
            this.Combo_Flowers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Combo_Flowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Combo_Flowers.FormattingEnabled = true;
            this.Combo_Flowers.Items.AddRange(new object[] {
            "Cherry Blossom",
            "Jasmine Flower",
            "Peach Blossom",
            "Orchid",
            "Balloon Flower",
            "Buttercup",
            "Rose",
            "Rondeletia"});
            this.Combo_Flowers.Location = new System.Drawing.Point(38, 129);
            this.Combo_Flowers.Name = "Combo_Flowers";
            this.Combo_Flowers.Size = new System.Drawing.Size(194, 28);
            this.Combo_Flowers.TabIndex = 0;
            this.Combo_Flowers.SelectedIndexChanged += new System.EventHandler(this.Combo_Flowers_SelectedIndexChanged);
            // 
            // Type: listBox   
            // Name: List_Trees
            // Function: Containing all items for Trees. Once clicked on item(s), event_Handler will retrieve and display the item(s) in TextBox_TreesList
            // 
            this.List_Trees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.List_Trees.FormattingEnabled = true;
            this.List_Trees.ItemHeight = 20;
            this.List_Trees.Items.AddRange(new object[] {
            "Maple",
            "Oak",
            "Palm",
            "Pine",
            "Coconut",
            "Dragon Tree",
            "Tree of Heaven",
            "White Willow"});
            this.List_Trees.Location = new System.Drawing.Point(265, 129);
            this.List_Trees.Name = "List_Trees";
            this.List_Trees.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.List_Trees.Size = new System.Drawing.Size(243, 84);
            this.List_Trees.TabIndex = 1;
            this.List_Trees.SelectedIndexChanged += new System.EventHandler(this.List_Trees_SelectedIndexChanged);
            // 
            // Type: Label
            // Name: Label_MYS
            // Function: Displays the title of the application "Make your selection"
            // 
            this.Label_MYS.AutoSize = true;
            this.Label_MYS.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_MYS.ForeColor = System.Drawing.Color.Yellow;
            this.Label_MYS.Location = new System.Drawing.Point(149, 32);
            this.Label_MYS.Name = "Label_MYS";
            this.Label_MYS.Size = new System.Drawing.Size(240, 29);
            this.Label_MYS.TabIndex = 2;
            this.Label_MYS.Text = "Mark your selection";
            // 
            // Type: Label
            // Name: Label_Flowers
            // Function: Represents the title of the comboBox 'Combo_Flowers'
            // 
            this.Lable_Flowers.AutoSize = true;
            this.Lable_Flowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Lable_Flowers.ForeColor = System.Drawing.Color.Yellow;
            this.Lable_Flowers.Location = new System.Drawing.Point(84, 97);
            this.Lable_Flowers.Name = "Lable_Flowers";
            this.Lable_Flowers.Size = new System.Drawing.Size(73, 22);
            this.Lable_Flowers.TabIndex = 3;
            this.Lable_Flowers.Text = "Flowers";
            // 
            // Type: Label
            // Name: Label_Trees
            // Function: Represents the title of the listBox 'List_Trees'
            // 
            this.Lable_Trees.AutoSize = true;
            this.Lable_Trees.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Lable_Trees.ForeColor = System.Drawing.Color.Yellow;
            this.Lable_Trees.Location = new System.Drawing.Point(342, 97);
            this.Lable_Trees.Name = "Lable_Trees";
            this.Lable_Trees.Size = new System.Drawing.Size(57, 22);
            this.Lable_Trees.TabIndex = 4;
            this.Lable_Trees.Text = "Trees";
            // 
            // Type: Label
            // Name: Label_FlowersPref
            // Function: Represents the title of the textBox 'TextBox_ComboFlowers'
            // 
            this.Lable_FlowersPref.AutoSize = true;
            this.Lable_FlowersPref.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Lable_FlowersPref.ForeColor = System.Drawing.Color.Yellow;
            this.Lable_FlowersPref.Location = new System.Drawing.Point(49, 333);
            this.Lable_FlowersPref.Name = "Lable_FlowersPref";
            this.Lable_FlowersPref.Size = new System.Drawing.Size(173, 22);
            this.Lable_FlowersPref.TabIndex = 5;
            this.Lable_FlowersPref.Text = "Flowers preferences";
            // 
            // Type: Label
            // Name: Label_TreesPref
            // Function: Represents the title of the textBox 'TextBox_ListTrees'
            // 
            this.Lable_TreesPref.AutoSize = true;
            this.Lable_TreesPref.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Lable_TreesPref.ForeColor = System.Drawing.Color.Yellow;
            this.Lable_TreesPref.Location = new System.Drawing.Point(303, 263);
            this.Lable_TreesPref.Name = "Lable_TreesPref";
            this.Lable_TreesPref.Size = new System.Drawing.Size(157, 22);
            this.Lable_TreesPref.TabIndex = 6;
            this.Lable_TreesPref.Text = "Trees preferences";
            // 
            // Type: textBox
            // Name: TextBox_ComboFlowers
            // Function: Display the selected item from the comboBox 'Combo_Flowers'
            // 
            this.TextBox_ComboFlowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TextBox_ComboFlowers.Location = new System.Drawing.Point(38, 366);
            this.TextBox_ComboFlowers.Name = "TextBox_ComboFlowers";
            this.TextBox_ComboFlowers.ReadOnly = true;
            this.TextBox_ComboFlowers.Size = new System.Drawing.Size(194, 26);
            this.TextBox_ComboFlowers.TabIndex = 7;
            // 
            // Type: textBox
            // Name: TextBox_ListTrees
            // Function: Display the selected item(s) from the listBox 'List_Trees'
            // 
            this.TextBox_ListTrees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.TextBox_ListTrees.Location = new System.Drawing.Point(265, 290);
            this.TextBox_ListTrees.Multiline = true;
            this.TextBox_ListTrees.Name = "TextBox_ListTrees";
            this.TextBox_ListTrees.ReadOnly = true;
            this.TextBox_ListTrees.Size = new System.Drawing.Size(243, 102);
            this.TextBox_ListTrees.TabIndex = 8;
            // 
            // Type: Form
            // Name: Gardening_Guide
            // Function: Contains all controlls and performances
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(539, 428);
            this.Controls.Add(this.TextBox_ListTrees);
            this.Controls.Add(this.TextBox_ComboFlowers);
            this.Controls.Add(this.Lable_TreesPref);
            this.Controls.Add(this.Lable_FlowersPref);
            this.Controls.Add(this.Lable_Trees);
            this.Controls.Add(this.Lable_Flowers);
            this.Controls.Add(this.Label_MYS);
            this.Controls.Add(this.List_Trees);
            this.Controls.Add(this.Combo_Flowers);
            this.Name = "Gardening_Guide";
            this.Text = "Gardening Guide";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Combo_Flowers;
        private System.Windows.Forms.ListBox List_Trees;
        private System.Windows.Forms.Label Label_MYS;
        private System.Windows.Forms.Label Lable_Flowers;
        private System.Windows.Forms.Label Lable_Trees;
        private System.Windows.Forms.Label Lable_FlowersPref;
        private System.Windows.Forms.Label Lable_TreesPref;
        private System.Windows.Forms.TextBox TextBox_ComboFlowers;
        private System.Windows.Forms.TextBox TextBox_ListTrees;
    }
}

