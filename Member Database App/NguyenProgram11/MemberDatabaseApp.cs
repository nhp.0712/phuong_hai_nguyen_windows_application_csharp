﻿/*  
    File:           MemberDatabaseApp.cs
    Programmer:     Phuong Nguyen
    Date:           April 27, 2019
    Purpose:        create a Windows application that uses a DataAdapter and DataSet to display and update the contents of an Access database using a DataGridView on the Form
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;        //Namespace for calsses to use with an Access database
namespace NguyenProgram11
{
    public partial class MemberDatabaseApp : Form
    {
        private string connectionString;    // To hold the connection string
        private string sqlCommand;  // To hold the SQL command
        private OleDbCommand dbCommand; // Command object for Access database
        private OleDbConnection dbConnect;  // Connection object for Access database
        private OleDbDataAdapter memberAdapter; // DataAdapter to facilitate usinga DataSet
        private DataSet memberDataSet;  // DataSet to contain member database data
        private OleDbCommandBuilder commandBuild; // To automatically generate SQL

        public MemberDatabaseApp()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Construct an OleDbConnection object using the appropriate connection string
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=member.accdb";
            dbConnect = new OleDbConnection(connectionString);

            // Assign the SQL command to select all records ordered by last, then first name
            sqlCommand = "SELECT * FROM memberTable ORDER BY LastName ASC, FirstName ASC;";

            // Construct an OleDbCommand object to hold the query text.
            // Then tie the command object to the connection object
            dbCommand = new OleDbCommand();
            dbCommand.CommandText = sqlCommand;
            dbCommand.Connection = dbConnect;

            // Construct an OleDbDataAdapter object to exchange data between the source
            // database and the DataSet object using the assigned select command
            memberAdapter = new OleDbDataAdapter();
            memberAdapter.SelectCommand = dbCommand;

            // Construct a DataSet object and fill with the member database data
            memberDataSet = new DataSet();
            memberAdapter.Fill(memberDataSet, "memberTable");

            // Connect the DataGridView to the DataSet
            dgdMember.DataSource = memberDataSet;
            dgdMember.DataMember = "memberTable";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // COnstruct a CommandBuilder object to automatically generate SQL
            commandBuild = new OleDbCommandBuilder(memberAdapter);
            //Use the DataAdapter's Update method to save changes from
            // the DataSet displayed in the DataGridViewto the source database
            memberAdapter.Update(memberDataSet, "memberTable");


        }
    }
}
