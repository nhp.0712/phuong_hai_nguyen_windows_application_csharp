﻿namespace NguyenProgram7
{
    partial class PresentationGUIwithDLLs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.laName = new System.Windows.Forms.Label();
            this.laAge = new System.Windows.Forms.Label();
            this.laID = new System.Windows.Forms.Label();
            this.laSleepAmt = new System.Windows.Forms.Label();
            this.laSleepAmtPpl = new System.Windows.Forms.Label();
            this.btnShow = new System.Windows.Forms.Button();
            this.txtBxName = new System.Windows.Forms.TextBox();
            this.txtBxAge = new System.Windows.Forms.TextBox();
            this.txtBxID = new System.Windows.Forms.TextBox();
            this.txtBxStudentSleep = new System.Windows.Forms.TextBox();
            this.txtBxPersonSleep = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // laName
            // 
            this.laName.AutoSize = true;
            this.laName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laName.ForeColor = System.Drawing.Color.Gold;
            this.laName.Location = new System.Drawing.Point(41, 29);
            this.laName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.laName.Name = "laName";
            this.laName.Size = new System.Drawing.Size(55, 20);
            this.laName.TabIndex = 0;
            this.laName.Text = "Name:";
            // 
            // laAge
            // 
            this.laAge.AutoSize = true;
            this.laAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laAge.ForeColor = System.Drawing.Color.Gold;
            this.laAge.Location = new System.Drawing.Point(41, 84);
            this.laAge.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.laAge.Name = "laAge";
            this.laAge.Size = new System.Drawing.Size(42, 20);
            this.laAge.TabIndex = 1;
            this.laAge.Text = "Age:";
            // 
            // laID
            // 
            this.laID.AutoSize = true;
            this.laID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laID.ForeColor = System.Drawing.Color.Gold;
            this.laID.Location = new System.Drawing.Point(41, 144);
            this.laID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.laID.Name = "laID";
            this.laID.Size = new System.Drawing.Size(91, 20);
            this.laID.TabIndex = 2;
            this.laID.Text = "Student ID:";
            // 
            // laSleepAmt
            // 
            this.laSleepAmt.AutoSize = true;
            this.laSleepAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laSleepAmt.ForeColor = System.Drawing.Color.Gold;
            this.laSleepAmt.Location = new System.Drawing.Point(184, 210);
            this.laSleepAmt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.laSleepAmt.Name = "laSleepAmt";
            this.laSleepAmt.Size = new System.Drawing.Size(112, 20);
            this.laSleepAmt.TabIndex = 3;
            this.laSleepAmt.Text = "Sleep amount:";
            // 
            // laSleepAmtPpl
            // 
            this.laSleepAmtPpl.AutoSize = true;
            this.laSleepAmtPpl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laSleepAmtPpl.ForeColor = System.Drawing.Color.Gold;
            this.laSleepAmtPpl.Location = new System.Drawing.Point(80, 253);
            this.laSleepAmtPpl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.laSleepAmtPpl.Name = "laSleepAmtPpl";
            this.laSleepAmtPpl.Size = new System.Drawing.Size(226, 20);
            this.laSleepAmtPpl.TabIndex = 4;
            this.laSleepAmtPpl.Text = "Sleep amount for most people:";
            // 
            // btnShow
            // 
            this.btnShow.BackColor = System.Drawing.Color.Navy;
            this.btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShow.ForeColor = System.Drawing.Color.Gold;
            this.btnShow.Location = new System.Drawing.Point(134, 301);
            this.btnShow.Margin = new System.Windows.Forms.Padding(2);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(135, 34);
            this.btnShow.TabIndex = 5;
            this.btnShow.Text = "Show Student";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // txtBxName
            // 
            this.txtBxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxName.Location = new System.Drawing.Point(134, 23);
            this.txtBxName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxName.Multiline = true;
            this.txtBxName.Name = "txtBxName";
            this.txtBxName.Size = new System.Drawing.Size(250, 26);
            this.txtBxName.TabIndex = 6;
            // 
            // txtBxAge
            // 
            this.txtBxAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxAge.Location = new System.Drawing.Point(134, 84);
            this.txtBxAge.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxAge.Multiline = true;
            this.txtBxAge.Name = "txtBxAge";
            this.txtBxAge.Size = new System.Drawing.Size(76, 27);
            this.txtBxAge.TabIndex = 7;
            // 
            // txtBxID
            // 
            this.txtBxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxID.Location = new System.Drawing.Point(134, 141);
            this.txtBxID.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxID.Multiline = true;
            this.txtBxID.Name = "txtBxID";
            this.txtBxID.Size = new System.Drawing.Size(136, 28);
            this.txtBxID.TabIndex = 8;
            // 
            // txtBxStudentSleep
            // 
            this.txtBxStudentSleep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxStudentSleep.Location = new System.Drawing.Point(308, 202);
            this.txtBxStudentSleep.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxStudentSleep.Multiline = true;
            this.txtBxStudentSleep.Name = "txtBxStudentSleep";
            this.txtBxStudentSleep.Size = new System.Drawing.Size(76, 28);
            this.txtBxStudentSleep.TabIndex = 9;
            // 
            // txtBxPersonSleep
            // 
            this.txtBxPersonSleep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBxPersonSleep.Location = new System.Drawing.Point(308, 253);
            this.txtBxPersonSleep.Margin = new System.Windows.Forms.Padding(2);
            this.txtBxPersonSleep.Multiline = true;
            this.txtBxPersonSleep.Name = "txtBxPersonSleep";
            this.txtBxPersonSleep.Size = new System.Drawing.Size(76, 28);
            this.txtBxPersonSleep.TabIndex = 10;
            // 
            // PresentationGUIwithDLLs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(410, 366);
            this.Controls.Add(this.txtBxPersonSleep);
            this.Controls.Add(this.txtBxStudentSleep);
            this.Controls.Add(this.txtBxID);
            this.Controls.Add(this.txtBxAge);
            this.Controls.Add(this.txtBxName);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.laSleepAmtPpl);
            this.Controls.Add(this.laSleepAmt);
            this.Controls.Add(this.laID);
            this.Controls.Add(this.laAge);
            this.Controls.Add(this.laName);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PresentationGUIwithDLLs";
            this.Text = "Student Record";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laName;
        private System.Windows.Forms.Label laAge;
        private System.Windows.Forms.Label laID;
        private System.Windows.Forms.Label laSleepAmt;
        private System.Windows.Forms.Label laSleepAmtPpl;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.TextBox txtBxName;
        private System.Windows.Forms.TextBox txtBxAge;
        private System.Windows.Forms.TextBox txtBxID;
        private System.Windows.Forms.TextBox txtBxStudentSleep;
        private System.Windows.Forms.TextBox txtBxPersonSleep;
    }
}

