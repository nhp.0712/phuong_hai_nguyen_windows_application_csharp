﻿/*  
    File:           PresentationGUIwithDLLs.cs
    Programmer:     Phuong Nguyen
    Date:           March 30, 2019
    Purpose:        create a client application to use the Student dll.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StudentNamespace;

namespace NguyenProgram7
{
    public partial class PresentationGUIwithDLLs : Form
    {
        private Student aStudent;

        public PresentationGUIwithDLLs()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            aStudent = new Student("123456789", "Phuong", "Nguyen", "CS", "1111");
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            // Uses Age property defined in thePerson class
            aStudent.Age = 25;

            // Calls overridden ToString() in Person class
            txtBxName.Text = aStudent.ToString();

            // Calls ToString() defined in object class
            txtBxAge.Text = aStudent.Age.ToString();

            // Calls ToString() defined in Student class
            txtBxID.Text = aStudent.StudentId;

            // Calls GetSleepAmt() defined in Student class
            txtBxStudentSleep.Text = Convert.ToString(aStudent.GetSleepAmt());

            // Calls method defined in Student class that
            // has calls to base.GetSleepAmt() in Person class
            txtBxPersonSleep.Text = Convert.ToString(aStudent.CallOverriddenGetSleepAmt());
        }
    }
}
