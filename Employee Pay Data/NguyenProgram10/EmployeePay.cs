﻿/*  
    File:           EmployeePay.cs
    Programmer:     Phuong Nguyen
    Date:           April 21, 2019
    Purpose:        Create an Employee Pay Data application following the guidelines for Programming Exercise #9 on pp. 905-906.
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace NguyenProgram10
{
    public partial class EmployeePay : Form
    {
        string operand1 = string.Empty;
        string operand2 = string.Empty;
        string result;

        StreamReader inFile;
        StreamWriter outFile;
        public EmployeePay()
        {
            InitializeComponent();
        }

        // openToolStripMenuItem_Click function
        // Open and retrieve all the information in the text file
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                inFile = new StreamReader(openFileDialog1.FileName);
                MessageBox.Show("File "+ openFileDialog1.FileName + " opened successfully!");

                string firstLine = inFile.ReadLine();
                string[] ArrFirst = firstLine.Split(' ');
                txtFirst.Text = ArrFirst[1];
                txtLast.Text = ArrFirst[2];

                string secondLine = inFile.ReadLine();
                string[] ArrSecond = secondLine.Split(' ');
                txtNumber.Text = ArrSecond[2];

                string thirdLine = inFile.ReadLine();
                string[] ArrThird = thirdLine.Split(' ');
                txtPayRate.Text = ArrThird[2].Substring(1);

                string fourthLine = inFile.ReadLine();
                string[] ArrFourth = fourthLine.Split(' ');
                txtHours.Text = ArrFourth[1];

                inFile.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot open file: " + ex.Message);
            }

        }

        // saveAsToolStripMenuItem_Click function
        // Save all the input into a text file
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                operand1 = txtPayRate.Text;
                operand2 = txtHours.Text;

                double opr1, opr2;
                double.TryParse(operand1, out opr1);
                double.TryParse(operand2, out opr2);

                if(opr2 > 40)
                    result = (opr1 * 40 + opr1 * (opr2-40)/2).ToString();
                else
                    result = (opr1 * opr2).ToString();

                saveFileDialog1.ShowDialog();
                string Value = "Employee: " + txtFirst.Text + " " + txtLast.Text 
                    + Environment.NewLine
                    + "Employee Number: " + txtNumber.Text
                    + Environment.NewLine
                    + "Pay Rate: " + "$" + txtPayRate.Text 
                    + Environment.NewLine
                    + "Hours: " + txtHours.Text
                    + Environment.NewLine
                    + "=> Amount Earned: " + "$" + result;

                outFile = new StreamWriter(saveFileDialog1.FileName);
                outFile.WriteLine(Value);

                MessageBox.Show("File " + saveFileDialog1.FileName + " saved successfully!");
                outFile.Close();
             }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot save file: " + ex.Message);
            }
        }

        // exitToolStripMenuItem_Click function
        // Exit the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // btnShow_Click function
        // Display all information of employee and amount earned once clicked
        private void btnShow_Click(object sender, EventArgs e)
        {

            operand1 = txtPayRate.Text;
            operand2 = txtHours.Text;

            double opr1, opr2;
            double.TryParse(operand1, out opr1);
            double.TryParse(operand2, out opr2);

            if (opr2 > 40)
                result = (opr1 * 40 + opr1 * (opr2 - 40) / 2).ToString();
            else
                result = (opr1 * opr2).ToString();

            MessageBox.Show("Employee: " + txtFirst.Text + " " + txtLast.Text
                    + Environment.NewLine
                    + "Employee Number: " + txtNumber.Text
                    + Environment.NewLine
                    + "Pay Rate: " + "$" + txtPayRate.Text
                    + Environment.NewLine
                    + "Hours: " + txtHours.Text
                    + Environment.NewLine
                    + "=> Amount Earned: " + "$" + result);
        }
    }
}
