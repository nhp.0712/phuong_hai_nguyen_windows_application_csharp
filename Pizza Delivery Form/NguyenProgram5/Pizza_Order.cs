﻿/*  
    File:           Pizza_Order.cs
    Programmer:     Phuong Nguyen
    Date:           March 2, 2019
    Purpose:        Create a Pizza Delivery application following the guidelines for Programming Exercise #10 on p. 698, including a TabControl displaying at least 3 tabs
*/

using System;
using System.Windows.Forms;

namespace NguyenProgram5
{
    public partial class Pizza_Order : Form
    {
        public Pizza_Order()
        {
            InitializeComponent();
        }

        private void clbTopping_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        // Function: bPlaceOrder_Click
        // Type: void
        // Display Order option displays order info (all user entries/selections) once clicked on Display Order of Process Menu
        private void bPlaceOrder_Click(object sender, EventArgs e)
        {
            // Calculating and initializing topping items and price
            string topping = "";
            double PriceTopping = 0;
            if (clbTopping.CheckedItems.Count != 0)
            {
                PriceTopping = clbTopping.CheckedItems.Count * 1.5;
                for (int x = 0; x < clbTopping.CheckedItems.Count; x++)
                {
                    topping = topping + " + " + clbTopping.CheckedItems[x].ToString();
                }
            }
            else
                topping = "None";

            // Calculating and initializing soft drinks items and price
            string softDrinks = "";
            double PriceSoft = 0;
            if (clbSoftDrinks.CheckedItems.Count != 0)
            {
                PriceSoft = clbSoftDrinks.CheckedItems.Count * 3.0;
                for (int x = 0; x < clbSoftDrinks.CheckedItems.Count; x++)
                {
                    softDrinks = softDrinks + " + " + clbSoftDrinks.CheckedItems[x].ToString();
                }
            }
            else
                softDrinks = "None";

            // Calculating and initializing hot drinks items and price
            string hotDrinks = "";
            double PriceHot = 0;
            if (clbHotDrinks.CheckedItems.Count != 0)
            {
                PriceHot = clbHotDrinks.CheckedItems.Count * 4.0;
                for (int x = 0; x < clbHotDrinks.CheckedItems.Count; x++)
                {
                    hotDrinks = hotDrinks + " + " + clbHotDrinks.CheckedItems[x].ToString();
                }
            }
            else
                hotDrinks = "None";

            // Calculating and initializing alcohol items and price
            string alcohol = "";
            double PriceAlcohol = 0;
            if (clbAlcohol.CheckedItems.Count != 0)
            {
                PriceAlcohol = clbAlcohol.CheckedItems.Count * 5.0;
                for (int x = 0; x < clbAlcohol.CheckedItems.Count; x++)
                {
                    alcohol = alcohol + " + " + clbAlcohol.CheckedItems[x].ToString();
                }
            }
            else
                alcohol = "None";

            // Calculating and initializing sides items and price
            string sides = "";
            double PriceSides = 0;
            if (clbSides.CheckedItems.Count != 0)
            {
                PriceSides = clbSides.CheckedItems.Count * 4.5;
                for (int x = 0; x < clbSides.CheckedItems.Count; x++)
                {
                    sides = sides + " + " + clbSides.CheckedItems[x].ToString();
                }
            }
            else
                sides = "None";

            // Calculating and initializing pizza size and price
            double PriceSize = 0;
            if (comSize.Text == "Small(10\'\') - $9.95")
                PriceSize = 9.95;
            else if (comSize.Text == "Medium (12\'\') - $12.95")
                PriceSize = 12.95;
            else if (comSize.Text == "Large (14\'\') - $14.95")
                PriceSize = 14.95;
            else if (comSize.Text == "Extra Large (16\'\') - $16.95")
                PriceSize = 16.95;

            // Calculating and initializing total price
            double PriceTotal = PriceSize + PriceTopping + PriceSoft + PriceHot + PriceAlcohol + PriceSides;

            // Using MessageBox to display all selected items and prices for each section, then display total price
            MessageBox.Show("Welcome to Cheesy Harry's Pizza!" + Environment.NewLine 
                            + "1445 Clough St, Bowling Green, Ohio 43403, U.S" + Environment.NewLine
                            + "567-413-6868" + Environment.NewLine + Environment.NewLine
                            + "Mr/Ms " + tName.Text + Environment.NewLine
                            + "Your Address: " + tAddress.Text + Environment.NewLine
                            + "Your Phone: " + tPhone.Text + Environment.NewLine
                            + "Your Email: " + tEmail.Text + Environment.NewLine
                            + "Your Oder Option: " + comOptions.Text + Environment.NewLine + Environment.NewLine
                            + "Here are your order details " + Environment.NewLine
                            + "=============================================" + Environment.NewLine
                            + "Size: " + comSize.Text + Environment.NewLine
                            + "Type of Crust: "+ comCrust.Text + Environment.NewLine
                            + "Topping(s): " + topping + " = $" + PriceTopping + Environment.NewLine
                            + "Soft Drinks: " + softDrinks + " = $" + PriceSoft + Environment.NewLine
                            + "Hot Drinks: " + hotDrinks + " = $" + PriceHot + Environment.NewLine
                            + "Alcohol: " + alcohol + " = $" + PriceAlcohol + Environment.NewLine
                            + "Sides: " + sides + " = $" + PriceSides + Environment.NewLine
                            + "=============================================" + Environment.NewLine
                            + "Total: $" + PriceTotal + Environment.NewLine 
                            + "(No Tax) - Welcome to our restaurant where we take care of your taxes :)" + Environment.NewLine + Environment.NewLine
                            + "Thank you and enjoy your meal !!");
        }

        // Function: bReset_Click
        // Type: void
        // Resest the application for users to make a new order
        // Resest the application for users to make a new order
        // Resest the application for users to make a new order
        // Resest the application for users to make a new order
        private void bReset_Click(object sender, EventArgs e)
        {
            Pizza_Order New_Oder = new Pizza_Order();
            New_Oder.Show();
            this.Dispose(false);
        }

        private void Pizza_Order_Load(object sender, EventArgs e)
        {
           
        }
    }
    
}
