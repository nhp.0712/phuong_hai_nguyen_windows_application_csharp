﻿/*  
    File:           Pizza_Order.Designer.cs
    Programmer:     Phuong Nguyen
    Date:           March 2, 2019
    Purpose:        Containing all functions and contents of the Windows Form Application
*/

namespace NguyenProgram5
{
    partial class Pizza_Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pizza_Order));
            this.tcMenus = new System.Windows.Forms.TabControl();
            this.tabPizza = new System.Windows.Forms.TabPage();
            this.picPizza = new System.Windows.Forms.PictureBox();
            this.clbTopping = new System.Windows.Forms.CheckedListBox();
            this.comCrust = new System.Windows.Forms.ComboBox();
            this.comSize = new System.Windows.Forms.ComboBox();
            this.laTopping = new System.Windows.Forms.Label();
            this.laCrust = new System.Windows.Forms.Label();
            this.laSize = new System.Windows.Forms.Label();
            this.laPizzaSelect = new System.Windows.Forms.Label();
            this.tabBeverages = new System.Windows.Forms.TabPage();
            this.picBeverages = new System.Windows.Forms.PictureBox();
            this.clbHotDrinks = new System.Windows.Forms.CheckedListBox();
            this.clbAlcohol = new System.Windows.Forms.CheckedListBox();
            this.clbSoftDrinks = new System.Windows.Forms.CheckedListBox();
            this.laHotDrinks = new System.Windows.Forms.Label();
            this.laAlcohol = new System.Windows.Forms.Label();
            this.laSoftDrinks = new System.Windows.Forms.Label();
            this.laDrinksSelect = new System.Windows.Forms.Label();
            this.tabSides = new System.Windows.Forms.TabPage();
            this.picSides = new System.Windows.Forms.PictureBox();
            this.clbSides = new System.Windows.Forms.CheckedListBox();
            this.laSidesPrice = new System.Windows.Forms.Label();
            this.laSidesSelect = new System.Windows.Forms.Label();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.comOptions = new System.Windows.Forms.ComboBox();
            this.tPhone = new System.Windows.Forms.TextBox();
            this.tAddress = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.laOptions = new System.Windows.Forms.Label();
            this.laPhone = new System.Windows.Forms.Label();
            this.laAddress = new System.Windows.Forms.Label();
            this.laName = new System.Windows.Forms.Label();
            this.bPlaceOrder = new System.Windows.Forms.Button();
            this.bReset = new System.Windows.Forms.Button();
            this.laEmail = new System.Windows.Forms.Label();
            this.tEmail = new System.Windows.Forms.TextBox();
            this.tcMenus.SuspendLayout();
            this.tabPizza.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPizza)).BeginInit();
            this.tabBeverages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBeverages)).BeginInit();
            this.tabSides.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSides)).BeginInit();
            this.tabInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMenus
            // 
            this.tcMenus.Controls.Add(this.tabPizza);
            this.tcMenus.Controls.Add(this.tabBeverages);
            this.tcMenus.Controls.Add(this.tabSides);
            this.tcMenus.Controls.Add(this.tabInfo);
            this.tcMenus.Location = new System.Drawing.Point(0, 0);
            this.tcMenus.Name = "tcMenus";
            this.tcMenus.SelectedIndex = 0;
            this.tcMenus.Size = new System.Drawing.Size(454, 380);
            this.tcMenus.TabIndex = 0;
            // 
            // tabPizza
            // 
            this.tabPizza.Controls.Add(this.picPizza);
            this.tabPizza.Controls.Add(this.clbTopping);
            this.tabPizza.Controls.Add(this.comCrust);
            this.tabPizza.Controls.Add(this.comSize);
            this.tabPizza.Controls.Add(this.laTopping);
            this.tabPizza.Controls.Add(this.laCrust);
            this.tabPizza.Controls.Add(this.laSize);
            this.tabPizza.Controls.Add(this.laPizzaSelect);
            this.tabPizza.Location = new System.Drawing.Point(4, 22);
            this.tabPizza.Name = "tabPizza";
            this.tabPizza.Padding = new System.Windows.Forms.Padding(3);
            this.tabPizza.Size = new System.Drawing.Size(446, 354);
            this.tabPizza.TabIndex = 0;
            this.tabPizza.Text = "Pizza";
            this.tabPizza.UseVisualStyleBackColor = true;
            // 
            // picPizza
            // 
            this.picPizza.Image = global::NguyenProgram5.Properties.Resources.pizza;
            this.picPizza.Location = new System.Drawing.Point(239, 175);
            this.picPizza.Name = "picPizza";
            this.picPizza.Size = new System.Drawing.Size(185, 136);
            this.picPizza.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picPizza.TabIndex = 7;
            this.picPizza.TabStop = false;
            // 
            // clbTopping
            // 
            this.clbTopping.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.clbTopping.FormattingEnabled = true;
            this.clbTopping.IntegralHeight = false;
            this.clbTopping.Items.AddRange(new object[] {
            "Pepperoni",
            "Mushroom",
            "Green Pepper",
            "Olives",
            "Chicken",
            "Ham",
            "Sausage",
            "Bacon",
            "Tomatoes",
            "Pineapples",
            "Onion",
            "Green Onion"});
            this.clbTopping.Location = new System.Drawing.Point(19, 175);
            this.clbTopping.Name = "clbTopping";
            this.clbTopping.Size = new System.Drawing.Size(174, 136);
            this.clbTopping.TabIndex = 6;
            this.clbTopping.SelectedIndexChanged += new System.EventHandler(this.clbTopping_SelectedIndexChanged);
            // 
            // comCrust
            // 
            this.comCrust.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comCrust.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.comCrust.FormattingEnabled = true;
            this.comCrust.Items.AddRange(new object[] {
            "Thin Crust Pizza Dough Recipe",
            "Pizza Hut Crust Recipe",
            "Papa John\'s Pizza Dough",
            "Grilled Pizza Crust",
            "Chicago Deep Dish Pizza Dough"});
            this.comCrust.Location = new System.Drawing.Point(239, 86);
            this.comCrust.Name = "comCrust";
            this.comCrust.Size = new System.Drawing.Size(185, 23);
            this.comCrust.TabIndex = 5;
            // 
            // comSize
            // 
            this.comSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.comSize.FormattingEnabled = true;
            this.comSize.Items.AddRange(new object[] {
            "Small (10\'\') - $9.95",
            "Medium (12\'\') - $12.95",
            "Large (14\'\') - $14.95",
            "Extra Large (16\'\') - $16.95"});
            this.comSize.Location = new System.Drawing.Point(19, 86);
            this.comSize.Name = "comSize";
            this.comSize.Size = new System.Drawing.Size(174, 26);
            this.comSize.TabIndex = 4;
            // 
            // laTopping
            // 
            this.laTopping.AutoSize = true;
            this.laTopping.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.laTopping.Location = new System.Drawing.Point(27, 143);
            this.laTopping.Name = "laTopping";
            this.laTopping.Size = new System.Drawing.Size(166, 20);
            this.laTopping.TabIndex = 3;
            this.laTopping.Text = "Toppings - $1.50 each";
            // 
            // laCrust
            // 
            this.laCrust.AutoSize = true;
            this.laCrust.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.laCrust.Location = new System.Drawing.Point(288, 60);
            this.laCrust.Name = "laCrust";
            this.laCrust.Size = new System.Drawing.Size(103, 20);
            this.laCrust.TabIndex = 2;
            this.laCrust.Text = "Type of Crust";
            // 
            // laSize
            // 
            this.laSize.AutoSize = true;
            this.laSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laSize.Location = new System.Drawing.Point(91, 60);
            this.laSize.Name = "laSize";
            this.laSize.Size = new System.Drawing.Size(40, 20);
            this.laSize.TabIndex = 1;
            this.laSize.Text = "Size";
            // 
            // laPizzaSelect
            // 
            this.laPizzaSelect.AutoSize = true;
            this.laPizzaSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laPizzaSelect.Location = new System.Drawing.Point(127, 17);
            this.laPizzaSelect.Name = "laPizzaSelect";
            this.laPizzaSelect.Size = new System.Drawing.Size(189, 29);
            this.laPizzaSelect.TabIndex = 0;
            this.laPizzaSelect.Text = "Pizza Selections";
            // 
            // tabBeverages
            // 
            this.tabBeverages.Controls.Add(this.picBeverages);
            this.tabBeverages.Controls.Add(this.clbHotDrinks);
            this.tabBeverages.Controls.Add(this.clbAlcohol);
            this.tabBeverages.Controls.Add(this.clbSoftDrinks);
            this.tabBeverages.Controls.Add(this.laHotDrinks);
            this.tabBeverages.Controls.Add(this.laAlcohol);
            this.tabBeverages.Controls.Add(this.laSoftDrinks);
            this.tabBeverages.Controls.Add(this.laDrinksSelect);
            this.tabBeverages.Location = new System.Drawing.Point(4, 22);
            this.tabBeverages.Name = "tabBeverages";
            this.tabBeverages.Padding = new System.Windows.Forms.Padding(3);
            this.tabBeverages.Size = new System.Drawing.Size(446, 354);
            this.tabBeverages.TabIndex = 1;
            this.tabBeverages.Text = "Beverages";
            this.tabBeverages.UseVisualStyleBackColor = true;
            // 
            // picBeverages
            // 
            this.picBeverages.Image = global::NguyenProgram5.Properties.Resources._4481;
            this.picBeverages.Location = new System.Drawing.Point(220, 192);
            this.picBeverages.Name = "picBeverages";
            this.picBeverages.Size = new System.Drawing.Size(203, 139);
            this.picBeverages.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBeverages.TabIndex = 7;
            this.picBeverages.TabStop = false;
            // 
            // clbHotDrinks
            // 
            this.clbHotDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.clbHotDrinks.FormattingEnabled = true;
            this.clbHotDrinks.Items.AddRange(new object[] {
            "Espresso",
            "Cafe latte",
            "Hot Tea",
            "Americano",
            "Hot Chocolate",
            "Cappucino"});
            this.clbHotDrinks.Location = new System.Drawing.Point(28, 227);
            this.clbHotDrinks.Name = "clbHotDrinks";
            this.clbHotDrinks.Size = new System.Drawing.Size(171, 80);
            this.clbHotDrinks.TabIndex = 6;
            // 
            // clbAlcohol
            // 
            this.clbAlcohol.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.clbAlcohol.FormattingEnabled = true;
            this.clbAlcohol.Items.AddRange(new object[] {
            "BudLight",
            "Heneiken",
            "Budweiser",
            "Blue Moon",
            "Guinness",
            "Corona",
            "Coors Light",
            "Miler Lite",
            "Vodka",
            "Johnnie Walker",
            "Macallan"});
            this.clbAlcohol.Location = new System.Drawing.Point(251, 94);
            this.clbAlcohol.Name = "clbAlcohol";
            this.clbAlcohol.Size = new System.Drawing.Size(172, 80);
            this.clbAlcohol.TabIndex = 5;
            // 
            // clbSoftDrinks
            // 
            this.clbSoftDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.clbSoftDrinks.FormattingEnabled = true;
            this.clbSoftDrinks.Items.AddRange(new object[] {
            "Coke",
            "Diet Coke",
            "Minute Maid",
            "RedBull",
            "Jasmine Tea",
            "Green Tea",
            "Root Beer",
            "Sprite"});
            this.clbSoftDrinks.Location = new System.Drawing.Point(28, 94);
            this.clbSoftDrinks.Name = "clbSoftDrinks";
            this.clbSoftDrinks.Size = new System.Drawing.Size(171, 80);
            this.clbSoftDrinks.TabIndex = 4;
            // 
            // laHotDrinks
            // 
            this.laHotDrinks.AutoSize = true;
            this.laHotDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.laHotDrinks.Location = new System.Drawing.Point(24, 204);
            this.laHotDrinks.Name = "laHotDrinks";
            this.laHotDrinks.Size = new System.Drawing.Size(176, 20);
            this.laHotDrinks.TabIndex = 3;
            this.laHotDrinks.Text = "Hot Drinks - $4.00 each";
            // 
            // laAlcohol
            // 
            this.laAlcohol.AutoSize = true;
            this.laAlcohol.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.laAlcohol.Location = new System.Drawing.Point(259, 62);
            this.laAlcohol.Name = "laAlcohol";
            this.laAlcohol.Size = new System.Drawing.Size(153, 20);
            this.laAlcohol.TabIndex = 2;
            this.laAlcohol.Text = "Alcohol - $5.00 each";
            // 
            // laSoftDrinks
            // 
            this.laSoftDrinks.AutoSize = true;
            this.laSoftDrinks.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.laSoftDrinks.Location = new System.Drawing.Point(24, 62);
            this.laSoftDrinks.Name = "laSoftDrinks";
            this.laSoftDrinks.Size = new System.Drawing.Size(180, 20);
            this.laSoftDrinks.TabIndex = 1;
            this.laSoftDrinks.Text = "Soft Drinks - $3.00 each";
            // 
            // laDrinksSelect
            // 
            this.laDrinksSelect.AutoSize = true;
            this.laDrinksSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.laDrinksSelect.Location = new System.Drawing.Point(122, 18);
            this.laDrinksSelect.Name = "laDrinksSelect";
            this.laDrinksSelect.Size = new System.Drawing.Size(200, 29);
            this.laDrinksSelect.TabIndex = 0;
            this.laDrinksSelect.Text = "Drinks Selections";
            // 
            // tabSides
            // 
            this.tabSides.Controls.Add(this.picSides);
            this.tabSides.Controls.Add(this.clbSides);
            this.tabSides.Controls.Add(this.laSidesPrice);
            this.tabSides.Controls.Add(this.laSidesSelect);
            this.tabSides.Location = new System.Drawing.Point(4, 22);
            this.tabSides.Name = "tabSides";
            this.tabSides.Padding = new System.Windows.Forms.Padding(3);
            this.tabSides.Size = new System.Drawing.Size(446, 354);
            this.tabSides.TabIndex = 2;
            this.tabSides.Text = "Sides";
            this.tabSides.UseVisualStyleBackColor = true;
            // 
            // picSides
            // 
            this.picSides.Image = global::NguyenProgram5.Properties.Resources.sides;
            this.picSides.Location = new System.Drawing.Point(255, 104);
            this.picSides.Name = "picSides";
            this.picSides.Size = new System.Drawing.Size(180, 194);
            this.picSides.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picSides.TabIndex = 3;
            this.picSides.TabStop = false;
            // 
            // clbSides
            // 
            this.clbSides.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.clbSides.FormattingEnabled = true;
            this.clbSides.Items.AddRange(new object[] {
            "Cheesy Bite Bites",
            "Jalapeno Poppers",
            "Cheesy Triangles",
            "Onion Rings",
            "Mini Corn on the Cob",
            "Garlic Bread",
            "Jack \'N\' Roll",
            "Fries",
            "Chicken Bites",
            "Garlic Bread with Mozzarella Cheese",
            "Garden Salad",
            "Spring Salad",
            "Spring Rolls",
            "Summer Rolls",
            "Buttermilk Cripsy Chicken Burger",
            "Coconut Icecream",
            "Brownies"});
            this.clbSides.Location = new System.Drawing.Point(19, 104);
            this.clbSides.Name = "clbSides";
            this.clbSides.Size = new System.Drawing.Size(211, 194);
            this.clbSides.TabIndex = 2;
            // 
            // laSidesPrice
            // 
            this.laSidesPrice.AutoSize = true;
            this.laSidesPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.laSidesPrice.Location = new System.Drawing.Point(30, 71);
            this.laSidesPrice.Name = "laSidesPrice";
            this.laSidesPrice.Size = new System.Drawing.Size(88, 20);
            this.laSidesPrice.TabIndex = 1;
            this.laSidesPrice.Text = "$4.50 each";
            // 
            // laSidesSelect
            // 
            this.laSidesSelect.AutoSize = true;
            this.laSidesSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.laSidesSelect.Location = new System.Drawing.Point(129, 19);
            this.laSidesSelect.Name = "laSidesSelect";
            this.laSidesSelect.Size = new System.Drawing.Size(182, 29);
            this.laSidesSelect.TabIndex = 0;
            this.laSidesSelect.Text = "Sides Selection";
            // 
            // tabInfo
            // 
            this.tabInfo.Controls.Add(this.tEmail);
            this.tabInfo.Controls.Add(this.laEmail);
            this.tabInfo.Controls.Add(this.comOptions);
            this.tabInfo.Controls.Add(this.tPhone);
            this.tabInfo.Controls.Add(this.tAddress);
            this.tabInfo.Controls.Add(this.tName);
            this.tabInfo.Controls.Add(this.laOptions);
            this.tabInfo.Controls.Add(this.laPhone);
            this.tabInfo.Controls.Add(this.laAddress);
            this.tabInfo.Controls.Add(this.laName);
            this.tabInfo.Location = new System.Drawing.Point(4, 22);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfo.Size = new System.Drawing.Size(446, 354);
            this.tabInfo.TabIndex = 3;
            this.tabInfo.Text = "Contact Information";
            this.tabInfo.UseVisualStyleBackColor = true;
            // 
            // comOptions
            // 
            this.comOptions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.comOptions.FormattingEnabled = true;
            this.comOptions.Items.AddRange(new object[] {
            "Pick-up",
            "Delivery",
            "At Restaurant"});
            this.comOptions.Location = new System.Drawing.Point(177, 285);
            this.comOptions.Name = "comOptions";
            this.comOptions.Size = new System.Drawing.Size(239, 28);
            this.comOptions.TabIndex = 7;
            // 
            // tPhone
            // 
            this.tPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.tPhone.Location = new System.Drawing.Point(177, 138);
            this.tPhone.Name = "tPhone";
            this.tPhone.Size = new System.Drawing.Size(239, 26);
            this.tPhone.TabIndex = 6;
            // 
            // tAddress
            // 
            this.tAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.tAddress.Location = new System.Drawing.Point(177, 65);
            this.tAddress.Name = "tAddress";
            this.tAddress.Size = new System.Drawing.Size(239, 26);
            this.tAddress.TabIndex = 5;
            // 
            // tName
            // 
            this.tName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.tName.Location = new System.Drawing.Point(177, 8);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(239, 26);
            this.tName.TabIndex = 4;
            // 
            // laOptions
            // 
            this.laOptions.AutoSize = true;
            this.laOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.laOptions.Location = new System.Drawing.Point(8, 289);
            this.laOptions.Name = "laOptions";
            this.laOptions.Size = new System.Drawing.Size(129, 24);
            this.laOptions.TabIndex = 3;
            this.laOptions.Text = "Order Options:";
            // 
            // laPhone
            // 
            this.laPhone.AutoSize = true;
            this.laPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.laPhone.Location = new System.Drawing.Point(8, 140);
            this.laPhone.Name = "laPhone";
            this.laPhone.Size = new System.Drawing.Size(66, 24);
            this.laPhone.TabIndex = 2;
            this.laPhone.Text = "Phone:";
            // 
            // laAddress
            // 
            this.laAddress.AutoSize = true;
            this.laAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.laAddress.Location = new System.Drawing.Point(8, 67);
            this.laAddress.Name = "laAddress";
            this.laAddress.Size = new System.Drawing.Size(80, 24);
            this.laAddress.TabIndex = 1;
            this.laAddress.Text = "Address:";
            // 
            // laName
            // 
            this.laName.AutoSize = true;
            this.laName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.laName.Location = new System.Drawing.Point(8, 8);
            this.laName.Name = "laName";
            this.laName.Size = new System.Drawing.Size(61, 24);
            this.laName.TabIndex = 0;
            this.laName.Text = "Name:";
            // 
            // bPlaceOrder
            // 
            this.bPlaceOrder.BackColor = System.Drawing.SystemColors.Info;
            this.bPlaceOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.bPlaceOrder.Location = new System.Drawing.Point(52, 399);
            this.bPlaceOrder.Name = "bPlaceOrder";
            this.bPlaceOrder.Size = new System.Drawing.Size(112, 39);
            this.bPlaceOrder.TabIndex = 4;
            this.bPlaceOrder.Text = "Place Order";
            this.bPlaceOrder.UseVisualStyleBackColor = false;
            this.bPlaceOrder.Click += new System.EventHandler(this.bPlaceOrder_Click);
            // 
            // bReset
            // 
            this.bReset.BackColor = System.Drawing.SystemColors.Info;
            this.bReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.bReset.Location = new System.Drawing.Point(285, 399);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(110, 39);
            this.bReset.TabIndex = 5;
            this.bReset.Text = "Reset Order";
            this.bReset.UseVisualStyleBackColor = false;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // laEmail
            // 
            this.laEmail.AutoSize = true;
            this.laEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.laEmail.Location = new System.Drawing.Point(8, 213);
            this.laEmail.Name = "laEmail";
            this.laEmail.Size = new System.Drawing.Size(80, 24);
            this.laEmail.TabIndex = 8;
            this.laEmail.Text = "Email:";
            // 
            // tEmail
            // 
            this.tEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.tEmail.Location = new System.Drawing.Point(177, 211);
            this.tEmail.Name = "tEmail";
            this.tEmail.Size = new System.Drawing.Size(239, 26);
            this.tEmail.TabIndex = 9;
            // 
            // Pizza_Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Tan;
            this.ClientSize = new System.Drawing.Size(451, 450);
            this.Controls.Add(this.bReset);
            this.Controls.Add(this.bPlaceOrder);
            this.Controls.Add(this.tcMenus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Pizza_Order";
            this.Text = "Cheesy Harrys\'s Pizza";
            this.Load += new System.EventHandler(this.Pizza_Order_Load);
            this.tcMenus.ResumeLayout(false);
            this.tabPizza.ResumeLayout(false);
            this.tabPizza.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPizza)).EndInit();
            this.tabBeverages.ResumeLayout(false);
            this.tabBeverages.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBeverages)).EndInit();
            this.tabSides.ResumeLayout(false);
            this.tabSides.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSides)).EndInit();
            this.tabInfo.ResumeLayout(false);
            this.tabInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMenus;
        private System.Windows.Forms.TabPage tabPizza;
        private System.Windows.Forms.TabPage tabBeverages;
        private System.Windows.Forms.Label laTopping;
        private System.Windows.Forms.Label laCrust;
        private System.Windows.Forms.Label laSize;
        private System.Windows.Forms.Label laPizzaSelect;
        private System.Windows.Forms.ComboBox comCrust;
        private System.Windows.Forms.ComboBox comSize;
        private System.Windows.Forms.CheckedListBox clbTopping;
        private System.Windows.Forms.PictureBox picPizza;
        private System.Windows.Forms.Label laHotDrinks;
        private System.Windows.Forms.Label laAlcohol;
        private System.Windows.Forms.Label laSoftDrinks;
        private System.Windows.Forms.Label laDrinksSelect;
        private System.Windows.Forms.CheckedListBox clbHotDrinks;
        private System.Windows.Forms.CheckedListBox clbAlcohol;
        private System.Windows.Forms.CheckedListBox clbSoftDrinks;
        private System.Windows.Forms.PictureBox picBeverages;
        private System.Windows.Forms.TabPage tabSides;
        private System.Windows.Forms.Label laSidesPrice;
        private System.Windows.Forms.Label laSidesSelect;
        private System.Windows.Forms.CheckedListBox clbSides;
        private System.Windows.Forms.PictureBox picSides;
        private System.Windows.Forms.Button bPlaceOrder;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.TabPage tabInfo;
        private System.Windows.Forms.ComboBox comOptions;
        private System.Windows.Forms.TextBox tPhone;
        private System.Windows.Forms.TextBox tAddress;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.Label laOptions;
        private System.Windows.Forms.Label laPhone;
        private System.Windows.Forms.Label laAddress;
        private System.Windows.Forms.Label laName;
        private System.Windows.Forms.TextBox tEmail;
        private System.Windows.Forms.Label laEmail;
    }
}

