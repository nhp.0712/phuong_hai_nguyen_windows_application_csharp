﻿/*  
    File:           Order.aspx.cs
    Programmer:     Phuong Nguyen
    Date:           May 8, 2019
    Purpose:        create a 3-page ASP.NET website for a pizza restaurant using ASP.NET templates and Visual Studio drag & drop tools as well as writing C# code to process & display user order data.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NguyenProgram13
{
    public partial class Contact : Page
    {
        // btnSubmi_Click function
        // Display customer's name, phone and all the selected items with the total price (including tax)
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            tTotal.Text = "";
            double total = 0;

            // Retrieve and display customer's name, phone and all the selected items.
            tTotal.Text += tName.Text + " - " + tPhone.Text + Environment.NewLine 
                + "===============================" + Environment.NewLine;

            // Using if statement to check if customer select any item in each section. Then retrieve and display the data
            if (ddl12Pizza.SelectedItem.Text != "") {
                tTotal.Text += ddl12Pizza.SelectedItem.Text + " Pizza - $" + ddl12Pizza.SelectedItem.Value + Environment.NewLine;
                total += Convert.ToDouble(ddl12Pizza.SelectedItem.Value);
            }
            if (ddlSlicePizza.SelectedItem.Text != "") {
                tTotal.Text += ddlSlicePizza.SelectedItem.Text + " Slice - $" + ddlSlicePizza.SelectedItem.Value + Environment.NewLine;
                total += Convert.ToDouble(ddlSlicePizza.SelectedItem.Value);
            }
            if (ddlPasta.SelectedItem.Text != "") {
                tTotal.Text += ddlPasta.SelectedItem.Text + " Pasta - $" + ddlPasta.SelectedItem.Value + Environment.NewLine;
                total += Convert.ToDouble(ddlPasta.SelectedItem.Value);
            }
            if (ddlHotDrinks.SelectedItem.Text != "") {
                tTotal.Text += ddlHotDrinks.SelectedItem.Text + " - $" + ddlHotDrinks.SelectedItem.Value + Environment.NewLine;
                total += Convert.ToDouble(ddlHotDrinks.SelectedItem.Value);
            }
            if (ddlColdDrinks.SelectedItem.Text != "") {
                tTotal.Text += ddlColdDrinks.SelectedItem.Text + " - $" + ddlColdDrinks.SelectedItem.Value + Environment.NewLine;
                total += Convert.ToDouble(ddlColdDrinks.SelectedItem.Value);
            }
            if (ddlBlendedDrinks.SelectedItem.Text != "") {
                tTotal.Text += ddlBlendedDrinks.SelectedItem.Text + " - $" + ddlBlendedDrinks.SelectedItem.Value + Environment.NewLine;
                total += Convert.ToDouble(ddlBlendedDrinks.SelectedItem.Value);
            }

            // Calculating total with tax
            total = total * 1.07;
           
            // Display total price with tax and thank you message.
            tTotal.Text += "===============================" + Environment.NewLine + "Total with Tax: $" + String.Format("{0:0.00}", total) + Environment.NewLine + Environment.NewLine
                + "Thank you for choosing us. Enjoy your meal!" + Environment.NewLine;
        }
    }
}