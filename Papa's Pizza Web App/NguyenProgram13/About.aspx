﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="NguyenProgram13.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.<img alt="Papa's Pizza Image" src="Content/Images/PPizza_Image_3.PNG" style="width: 243px; height: 168px" /></h2>
    <h3>About Papa&#39;s Pizza </h3>
    <p><span style="font-weight: 400;">Pizza was relatively unknown until 1889. Till Queen Margherita and King Umberto I &nbsp;toured their kingdom, and the queen noticed this delicious peasant dish. She adored it! Soon, her personal chef was creating a series of pizzas – all according to her personal tastes. The chef won her over with a creation that saluted the colours of the Italian flag.</span></p>
</asp:Content>
