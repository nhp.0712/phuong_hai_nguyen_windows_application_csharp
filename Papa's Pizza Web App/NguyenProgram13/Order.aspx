﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="NguyenProgram13.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.Order for Pickup</h2>
    <h3>Enter Your Contact Info</h3>
<p>Name:
    <asp:TextBox ID="tName" runat="server" Width="249px"></asp:TextBox>
</p>
<p>Phone:
    <asp:TextBox ID="tPhone" runat="server" Width="219px"></asp:TextBox>
</p>
    <h3>Select your order</h3>
<p><strong style="font-size: 18px">12” Gourmet Pizza</strong></p>
<p>
    <asp:DropDownList ID="ddl12Pizza" runat="server" Height="22px" Width="327px">
        <asp:ListItem> </asp:ListItem>
        <asp:ListItem Value="10.00">Four Cheese</asp:ListItem>
        <asp:ListItem Value="10.00">Wild Mushrooms, tomato, mozzarella</asp:ListItem>
        <asp:ListItem Value="12.00">Pepperoni, mozzarella, Bolognese sauce </asp:ListItem>
        <asp:ListItem Value="13.00">Italian sausage, fontina, mozzarella </asp:ListItem>
        <asp:ListItem Value="14.00">Artichoke hearts, black olives, prosciutto </asp:ListItem>
        <asp:ListItem Value="14.00">Roast chicken, ricotta, spinach </asp:ListItem>
    </asp:DropDownList>
</p>
<p><strong style="font-size: 18px">Pizza by the Slice</strong></p>
<p>
    <asp:DropDownList ID="ddlSlicePizza" runat="server" Height="22px" Width="331px">
        <asp:ListItem> </asp:ListItem>
        <asp:ListItem Value="2.50">Four Cheese</asp:ListItem>
        <asp:ListItem Value="2.50">Wild Mushroom </asp:ListItem>
        <asp:ListItem Value="2.85">Pepperoni</asp:ListItem>
    </asp:DropDownList>
</p>
<p><strong style="font-size: 18px">Pasta</strong></p>
<p>
    <asp:DropDownList ID="ddlPasta" runat="server" Height="21px" Width="333px">
        <asp:ListItem> </asp:ListItem>
        <asp:ListItem Value="10.00">Spaghetti Pomodoro </asp:ListItem>
        <asp:ListItem Value="12.00">Ravioli de Ricotta </asp:ListItem>
        <asp:ListItem Value="13.00">Chicken Marsala</asp:ListItem>
        <asp:ListItem Value="14.00">Veal Parmigiana </asp:ListItem>
        <asp:ListItem Value="16.00">Seafood Scampi</asp:ListItem>
    </asp:DropDownList>
</p>
<p><strong style="font-size: 18px">Hot Drinks</strong></p>
<p>
    <asp:DropDownList ID="ddlHotDrinks" runat="server" Height="19px" Width="334px">
        <asp:ListItem> </asp:ListItem>
        <asp:ListItem Value="2.50">Café Latte</asp:ListItem>
        <asp:ListItem Value="2.50">Cappuccino </asp:ListItem>
        <asp:ListItem Value="2.50">Espresso</asp:ListItem>
        <asp:ListItem Value="2.50">Hot Chocolate</asp:ListItem>
        <asp:ListItem Value="2.50">Hot Tea</asp:ListItem>
    </asp:DropDownList>
</p>
<p><strong style="font-size: 18px">Cold Drinks</strong></p>
<p>
    <asp:DropDownList ID="ddlColdDrinks" runat="server" Height="18px" Width="336px">
        <asp:ListItem> </asp:ListItem>
        <asp:ListItem Value="3.00">Italian Soda</asp:ListItem>
        <asp:ListItem Value="3.00">Iced Coffee</asp:ListItem>
        <asp:ListItem Value="3.00">Iced Tea  </asp:ListItem>
    </asp:DropDownList>
</p>
<p><strong style="font-size: 18px">Blended Drinks</strong></p>
<p>
    <asp:DropDownList ID="ddlBlendedDrinks" runat="server" Height="16px" Width="337px">
        <asp:ListItem> </asp:ListItem>
        <asp:ListItem Value="3.75">Fruit Smoothie </asp:ListItem>
        <asp:ListItem Value="3.75">Vanilla Freeze</asp:ListItem>
        <asp:ListItem Value="3.75">Mocha Freeze</asp:ListItem>
    </asp:DropDownList>
</p>
<p>
    <asp:Button ID="btnSubmit" runat="server" Font-Bold="True" Font-Size="X-Large" Height="51px" OnClick="btnSubmit_Click" Text="Submit Order" Width="276px" />
</p>
<p><strong style="font-size: 18px">Order Total</strong></p>
<p>
    <asp:TextBox ID="tTotal" runat="server" Height="199px" TextMode="MultiLine" Width="42%"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>
    </asp:Content>
